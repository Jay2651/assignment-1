﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BlogWeb.Models;
namespace BlogWeb.Controllers
{
    public class BlogController : Controller
    {
        blogdbEntities1 db = new blogdbEntities1();
        //
        // GET: /Blog/
        public ActionResult Index()
        {
            return View(db.blogtbs.ToList());
        }
        [HttpPost]
        public ActionResult Index(int[] chkid)
        {
            foreach (int itemid in chkid)
            {
                var findd = db.blogtbs.Where(x => x.id == itemid).FirstOrDefault();
                var deletee = db.blogtbs.Remove(findd);
                
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult insert()
        {
            return View();
        }
        [HttpPost]
        public ActionResult insert(HttpPostedFileBase file, blogtb tb, string blog_title, string blog_description)
        {
            string image_name = System.IO.Path.GetFileName(file.FileName);
            string path_name = Server.MapPath("~/images/" + image_name);
            file.SaveAs(path_name);

            tb.blog_image = image_name;
            tb.blog_title = blog_title;
            tb.blog_description = blog_description;
            db.blogtbs.Add(tb);
            db.SaveChanges();
            ViewBag.mess = "blog inserted success";

            return View();
        }
        public ActionResult select(int id)
        {
            return View(db.blogtbs.Where(x=>x.id==id).ToList());
        }
        [HttpPost]
        public ActionResult select(int id, string blog_title, string blog_description)
        {
            var update = db.blogtbs.Where(x => x.id == id).FirstOrDefault();
            update.blog_title = blog_title;
            update.blog_description = blog_description;
            db.SaveChanges();

            return RedirectToAction("Index");

        }
	}
}