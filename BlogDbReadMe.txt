create database blogdb

use blogdb

create table blogtb
(
id int identity primary key,
blog_image varchar(500),
blog_title varchar(200),
blog_description varchar(800)
)
drop table blogtb
select * from blogtb